var app = getApp()
function generateUUID() {
  let d = new Date().getTime();
  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = (d + Math.random()*16)%16 | 0;
    d = Math.floor(d/16);
    return (c=='x' ? r : (r&0x3|0x8)).toString(16);
  });
  return uuid;
}

Page({
  data: {
    globalData:{
      StatusBar:'',
      Custom:'',
      CustomBar:'',
    },
    newsList:[],
    openid:'',
    figureurl_wx:'',
    animation:false,
    voice:false,
    sendout:false,
    camera:false,
    news:'',
    pression:[],
    emojis:false,
    curra:0,
    feature:[{ src: '/images/album.png', name: '相册' }],
    alcur:false,
    vision:'',
    time:'',
    duration:'',
    playing:false,
    cuindex:'',
    aiAnswer:'',
    conversation_id:'',
    shouldScrollToBottom: true,
    isUserScrolling: false,
  },

  fanhui:function(e){
    wx.removeStorage({
      key: 'newsList',
      success: function(res) {}
    });
    wx.redirectTo({
      url: '../index/index',
    })
  },

  cuinbut:function(e){
    let that = this
    setTimeout(function () {
      wx.createSelectorQuery().select('#chat-height').boundingClientRect(function (rect) {
        that.setData({
          chatheight: parseFloat(rect.height) + parseFloat(that.data.curra),
          curr: 'jump' + JSON.stringify(that.data.newsList.length - 1)
        })
        if (!that.data.isUserScrolling && that.data.shouldScrollToBottom) {
          that.scrollToBottom();
        }
      }).exec()
    }, 100);
    console.log(wx.getStorageSync('newsList'));
  },

  scrollToBottom: function() {
    this.setData({
      shouldScrollToBottom: true
    });
    setTimeout(() => {
      this.setData({
        shouldScrollToBottom: false
      });
    }, 300); // 在滚动动画完成后关闭滚动到底部
  },

  onShow:function(e){
    this.setData({
      curr: 'jump' + JSON.stringify(this.data.newsList.length-1)
    })
  },

  onShareAppMessage: function() {},

  onLoad: function (options) {
    let that = this;
    that.getUseridFromStorage();
    wx.setStorageSync('conversation_id', generateUUID())
    wx.getSystemInfo({
      success: e => {
        that.data.globalData.StatusBar = e.statusBarHeight - 20;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          that.data.globalData.Custom = capsule;
          that.data.globalData.CustomBar = capsule.bottom + capsule.top - (e.statusBarHeight + 20);
        } else {
          that.data.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    });
    that.data.figureurl_wx = options.figureurl_wx;
    const initList = [{
      news_type: 'text',
      news_centent: "科学养护宠儿，好开心您有问题咨询我",
      openid: 'ai',
      logo:''
    }]
    that.setData({
      openid: options.openid,
      globalData: that.data.globalData,
      newsList: initList,
      vision: wx.getRecorderManager(),
    });
  },

  getUseridFromStorage:function() {
    var self = this;
    wx.getStorage({
      key: 'userinfo',
      success (res) {
        self.data.user_id = res.data.id;
        self.data.figureurl_wx = res.data.figureurl_wx;
        self.data.openid = res.data.nickname;
      }
    });
  },

  getkey:function(e){
    this.setData({
      curra: e.detail.height,
      emojis: false,
      camera: false
    })
    this.cuinbut();
  },

  getblur:function(e){
    this.setData({
      curra: 0,
      emojis: false,
      camera: false
    })
    this.cuinbut();
  },

  moninput:function(e){
    let that = this
    that.cuinbut();
    this.setData({
      sendout: e.detail.value?true:false,
      news: e.detail.value,
    })
  },

  scrollstart:function(e){
    this.setData({
      emojis: false,
      camera: false,
      isUserScrolling: true
    })
    this.cuinbut();
  },

  scrollend:function(e){
    this.setData({
      isUserScrolling: false,
      shouldScrollToBottom: false
    })
  },

  onscroll:function(e) {
    if (this.data.isUserScrolling) {
      this.setData({
        shouldScrollToBottom: false
      });
    }
  },

  speak:function(e){
    this.setData({
      voice: !this.data.voice,
      emojis:false,
      camera:false,
    })
    this.cuinbut();
  },

  emoji:function(e){
    this.setData({
      emojis: !this.data.emojis,
      voice:false,
      camera:false,
    })
    this.cuinbut();
  },

  camerax:function(e){
    this.setData({
      camera: !this.data.camera,
      voice: false,
      emojis: false,
    })
    this.cuinbut();
  },

  upload:function(e){
    const that = this
    wx.chooseImage({
      count: 1,
      success: function (res) {
        const src = res.tempFilePaths[0];
        wx.getImageInfo({
          src,
          success: function (res) {
            const { width, height } = res
            const newChatList = [...that.data.newsList,{
              news_type: 'image',
              news_centent: { src, width, height },
              openid: that.data.openid,
              logo:that.data.figureurl_wx
            }];
            that.aiChat('');
            that.setData({
              animation: true,
              newsList: newChatList,
            });
            wx.setStorageSync('newsList', newChatList);
            that.cuinbut();
            that.aiChat();
          }
        })
      }
    })
  },

  picture:function(e){
    let src = e.currentTarget.dataset.src;
    wx.previewImage({
      current: src,
      urls: [src]
    })
  },

  message:function(e){
    let that = this
    if (that.data.news === '') {
      wx.showToast({
        title: '请输入内容',
        icon: 'none'
      })
      return;
    }
    const newChatList = [...that.data.newsList, {
      news_type: 'text',
      news_centent: that.data.news,
      openid: that.data.openid,
      logo:that.data.figureurl_wx
    }, {
      news_type: 'text',
      news_centent: "请稍等.....",
      openid: 'ai',
      logo:'https://guanchao.site/uploads/atricle/5c0a66cac42cb.jpeg'
    }];

    that.aiChat(that.data.news);
  
    that.setData({
      animation: true,
      newsList: newChatList,
      sendout:false,
      news: '',
      shouldScrollToBottom: true,
      isUserScrolling: false
    })
    wx.setStorageSync('newsList', newChatList);
    that.cuinbut();
  },

  expression:function(e){
    let item = e.currentTarget.dataset.item
    this.setData({
      sendout: true,
      news: this.data.news += item,
    })
  },

  featch:function(e){
    let that = this
    let index = e.currentTarget.dataset.index
    if(index == 0){
      that.upload();
    }
  },

  touchdown: function (e) {
    let that = this
    wx.stopVoice();
    that.setData({
      alcur: true,
      playing: false,
      cuindex: ''
    })
    that.data.duration = 0
    that.data.time = setInterval(function () {
      that.setData({
        duration: that.data.duration+1
      })
    },1000);
    wx.startRecord({
      success: function (res) {
        const newChatList = [...that.data.newsList, {
          news_type: 'voice',
          news_centent: { voice: res.tempFilePath, dimen: that.data.duration},
          openid: that.data.openid,
          logo:that.data.figureurl_wx
        }];
        that.aiChat('');
        that.setData({
          newsList: newChatList,
        })
        wx.setStorageSync('newsList', newChatList);
        that.cuinbut();
        that.aiChat();
      },
      fail:function(e){
        that.touchup();
      }
    });
    console.log(wx.getStorageSync('newsList'));
  },

  touchup: function (e) {
    let that = this
    wx.stopRecord()
    clearInterval(that.data.time)
    that.setData({
      alcur:false
    })
  },

  play: function (e) {
    let that = this
    let voice = e.currentTarget.dataset.voice
    var index = e.currentTarget.dataset.index
    if (!that.data.playing){
      that.setData({
        playing:true,
        cuindex:index
      })
      wx.playVoice({
        filePath: voice,
        success: function () {
          that.setData({
            playing: false,
            cuindex:''
          })
        },
      })
    }
  },

  suspend:function(e){
    wx.stopVoice();
    this.setData({
      playing: false,
      cuindex: ''
    })
  },

  aiChat: function(str) {
    var self = this;
    var conversation_id = wx.getStorageSync('conversation_id')
    wx.request({
      url: 'https://chat.myscale.cloud/api/chat',
      method: 'POST',
      header: {
        "X-API-KEY": "eyQtRXly91PddWiSPt8xKgtEOU6XVUgvVuYiX4sF1pAKx48Q",
        "Accept-Type": "application/json"
      },
      data: {
        "collection_id": "439693e9-b165-4c1d-a3d3-efe00e2edc5c",
        "conversation_id": conversation_id,
        "message": str
      },
      success(result){
        self.data.aiAnswer = result.data.message;
        const newElement = {
          news_type: 'text',
          news_centent: self.data.aiAnswer,
          openid: 'ai',
          logo:'https://guanchao.site/uploads/atricle/5c0a66cac42cb.jpeg'
        }
        const tailIndex = self.data.newsList.length - 1;
        self.data.newsList[tailIndex] = newElement;
        const newChatList = self.data.newsList;
        self.setData({
          animation: true,
          newsList: newChatList,
          sendout:false,
          news: '',
          shouldScrollToBottom: true,
          isUserScrolling: false
        })
        wx.setStorageSync('newsList', newChatList);
        self.cuinbut();
      }
    });
  },

  clicks:function(){
    console.log('12345');
  }
})
